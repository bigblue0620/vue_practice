export interface Wbs {
    columns: Column[];
    rows: Row[];
}

export interface Column {
    key: string;
    name: string;
    completed?: boolean;
    insertBefore?: boolean;
    bkey?: string; // 컬럼 왼쪽/또는 오른쪽 추가시 기준이 되는 컬럼의 키값
}

export interface Row {
    key: string;
    tds: RowTD[];
    colspan?: boolean;
    insertBefore?: boolean;
    bkey?: string;
}

type TDType = string | Date ;

export interface RowTD {
    ckey: string;
    rkey: string;
    key: string;
    name: string;
    isDate?: boolean;
}