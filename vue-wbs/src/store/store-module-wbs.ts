import { Commit, Dispatch, Module } from 'vuex';
import { Wbs, Column, Row, RowTD } from '../types';
import { v4 as uuidv4 } from 'uuid';

const wbs = {
    namespaced: true,
    state: () => ({
        columns: [],
        rows: []
    }),
    mutations: {
        updateColumn(state: Wbs, payload: Column) {
            const { columns, rows } = state;
            const idx = columns.findIndex((c) => c.key === payload.key);

            if (idx > -1) { // 기존 컬럼 수정
                state.columns[idx] = { ...payload };
            } else if (typeof payload.bkey !== 'undefined') { // 컬럼 좌우방향으로 추가시
                const idx = columns.findIndex((c) => c.key === payload.bkey);
                const insertIdx = payload.insertBefore ? idx:  idx + 1;
                state.columns.splice(insertIdx, 0, payload);

                // 컬럼 추가시 각 row에 컬럼란에 매치되는 td 생성
                for(const item of rows) {
                    const insertTd = { ckey: payload.key, rkey: item.key, key: uuidv4(), name: '' };
                    if (payload.insertBefore) {
                        const tdIdx = item.tds.findIndex((td) => td.ckey === payload.bkey);
                        if (tdIdx > -1) {
                            item.tds.splice(tdIdx, 0, insertTd);
                        }
                    } else {
                        item.tds.push(insertTd);
                    }
                }
            } else { // 초기 컬럼 생성시
                state.columns.push(payload);
            }
        },
        clear(state: Wbs) {
            state.columns = [];
            state.rows = [];
        },
        clearEmptyColumn(state: Wbs) {
            state.columns = state.columns.filter((c) => c.name);
            state.rows = state.rows.reduce((acc: Row[], cur: Row) => {
                const tds = cur.tds.filter((td) => td.name);
                if (tds.length !== 0) {
                    acc.push(cur);
                }
                return acc
            }, []);
        },
        updateRow(state: Wbs, payload: Row) {
            const { rows } = state;
            const idx = rows.findIndex((r) => r.key === payload.key);
            if (idx > -1) {
                state.rows[idx] = { ...payload };
            } else if (typeof payload.bkey !== 'undefined') {
                const idx = rows.findIndex((r) => r.key === payload.bkey);
                const insertIdx = payload.insertBefore ? idx:  idx + 1;
                state.rows.splice(insertIdx, 0, payload);
            } else {
                state.rows.push(payload);
            }
        },
        updateTd(state: Wbs, payload: RowTD) {
            const { rows } = state;
            const ridx = rows.findIndex((r) => r.key === payload.rkey);
            if (ridx > -1) {
                const tdIdx = rows[ridx].tds.findIndex((td) => td.ckey === payload.ckey);
                if (tdIdx > -1) {
                    rows[ridx].tds[tdIdx] = {...payload};
                }
            } 
        }
    },
    getters: {
        columns: (state: Wbs) => state.columns,
        // completedColumns: (state: Wbs) => state.columns.filter((c) => c.completed)
        rows: (state: Wbs) => state.rows,
    },
    actions: {
        setColumn({ commit }: { commit: Commit }, payload: Column) {
            commit('updateColumn', payload);
        },
        setRow({ commit }: { commit: Commit }, payload: Column) {
            commit('updateRow', payload);
        },
        setTd({ commit }: { commit: Commit }, payload: RowTD) {
            commit('updateTd', payload);
        },
        allClear({ commit }: { commit: Commit }) {
            commit('clear');
        },
        clearEmptyColumn({ commit }: { commit: Commit }) {
            commit('clearEmptyColumn');
        }
    }
};

export default wbs;
