import { createStore } from 'vuex';
import wbs from './store-module-wbs';
import createPersistedState from 'vuex-persistedstate';

const store = createStore({
    modules: { wbs },
    plugins: [
        createPersistedState({
            paths: ['wbs']
        })
    ]
});

export default store;
