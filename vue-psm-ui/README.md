# vue-psm-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

<br/>

## Code Sample

### v-on:input으로 한글입력오류 해결 (v-model 사용시 한글 IME 문제)

```
<input type="text" :value="name" @input="name=$event.target.value">

data() {
    return {
      name:''
    }
  }

```
```
<input type="text" :value="name" @input="changeName">

data() {
    return {
      name:''
    }
  },

methods: {
    changeName(e) {
      this.name = e.target.value
    }
  }
```