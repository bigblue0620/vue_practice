import App from './App.vue';
import { createApp } from 'vue';
import router from './router';
import axios from './utils/axios';
import store from './store';
import i18n from './i18n';
import vuetify from './plugins/vuetify';
// import { loadFonts } from './plugins/webfontloader';
// loadFonts();

const app = createApp(App);
app.config.globalProperties.axios = axios;

app.use(i18n).use(router).use(store).use(vuetify);
app.mount('#app');
