import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles';
import { aliases, mdi } from 'vuetify/lib/iconsets/mdi-svg';
import cssVar from '@/assets/scss/variables.module.scss';
import { createVuetify } from 'vuetify';

const CustomLightTheme = {
    dark: false,
    colors: {
        background: '#FFFFFF',
        surface: '#FFFFFF',
        primary: cssVar.colorPrimary,
        // 'primary-darken-1': '#3700B3',
        primaryLighten5: cssVar.colorPrimaryLighten5,
        secondary: cssVar.colorSecondary,        
        // 'secondary-darken-1': '#018786',
        error: cssVar.errorPrimary,
        // info: '#2196F3',
        // success: '#4CAF50',
        // warning: '#FB8C00',
        borderColorDefault: cssVar.borderColorDefault,
        borderColorHover: cssVar.borderColorHover,
        borderColorActive: cssVar.borderColorActive,
    }
};

export default createVuetify({
    icons: {
        defaultSet: 'mdi',
        aliases,
        sets: {
            mdi
        }
    },
    theme: {
        defaultTheme: 'CustomLightTheme',
        themes: {
            CustomLightTheme,
        }
    }
});
