import { Component } from "vue";

export interface LoginInfo {
    [index: string]: boolean | UserProfile | null | undefined;
    isLoggedIn: boolean;
    isInitialized: boolean;
    user?: UserProfile | null | undefined;
}

export interface UserProfile {
    id?: string;
    avatar?: string;
    image?: string;
    username?: string;
    name?: string;
    role?: string;
    about?: string;
    email?: string;
    work_email?: string;
    personal_email?: string;
    phone?: string;
    work_phone?: string;
    personal_phone?: string;
    birthdayText?: string;
    lastMessage?: string;
    status?: string;
    friends?: number;
    followers?: number;
    contact?: string;
    company?: string;
    location?: string;
    online_status?: string;
    unReadChatCount?: number;
    groups?: Group[];
    time?: string;
    tier?: string;
    Progress?: ProfileProgress;
}

interface Group {
    id: string;
    avatar: string;
    name: string;
}

interface ProfileProgress {
    label: string;
    variant: string;
    value: number;
}

export interface ServiceToken {
    apikey: string;
    time: { [key: string]: string | null };
    username: string;
}

export interface HostState {
    hosts: HostEntry[];
    addDialog?: boolean;
}

interface HostSettingState {
    initialize?: {
        status: string;
        message: string;
    };
    network?: {
        status: string;
        message: string;
    };
}

export interface HostEntry {
    [index: string]: string | undefined | HostSettingState;
    address: string;
    username: string;
    password: string;
    sshPort: string;
    ipmiAddress: string;
    ipmiUsername: string;
    ipmiPassword: string;
    hostname: string;
    status: string;
    ipmiStatus: string;
    powerStatus: string;
    initializeStatus: string | undefined;
    networkStatus?: string;
    state?: HostSettingState;
    HostUuid: string;
}

export interface Menu {
    id: string;
    type: string;
    label: string | string[];
    icon?: string;
    children?: Menu[];
    path?: [];
    value?: boolean;
}

// export type CommonObj = Record<string, boolean | string | number | unknown>;

export type CommonListType = { [index: string]: any, selected?: boolean }[];

export interface CommonTableObj {
    total: number;
    list: CommonListType;
}

export type TableDataType = Pick<CommonTableObj, 'list'>;

export interface ActionConfig {
    primary: PrimaryActions[] | undefined;
    rowAction: RowAction;
}

export interface PrimaryActions { // button type으로 고정함
    label?: string[];
    component: Component | undefined;
}

export interface RowActions {
    key: string;
    renderType: 'button' | 'text' | 'icon' | 'custom';
    actType: 'dialog' | 'function' | 'link';
    component: Component | undefined;   // required if actType is dialog or custom
    url?: string;                       // required if actType is link
    label?: string;                     // required if renderType is text
    tooltip?: string;                   // optional         
    icon?: string;                      // required if renderType is icon
    iconBgColor?: string;               // required if renderType is icon
    iconBgHoverColor?: string;          // required if renderType is icon
    iconColor?: string;                 // required if renderType is icon
    iconHoverColor?: string;            // required if renderType is icon
}

export interface RowAction {
    type: 'inline' | 'select' | undefined;  // inline, select(=skyline row action ui), select 타입일 경우 action은 전부 text 로 간주함
    actions: RowActions[];
}
