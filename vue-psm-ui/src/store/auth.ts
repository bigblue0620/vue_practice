import axios from '@/utils/axios';
import { LoginInfo, ServiceToken } from '@/types';
import { Commit, Dispatch, Module } from 'vuex';
import router from '../router';
import { AxiosError } from 'axios';

const auth: Module<LoginInfo, null> = {
    namespaced: true,
    state: () => ({
        isLoggedIn: false,
        isInitialized: false,
        user: null
    }),
    mutations: {
        update(state: LoginInfo, payload: LoginInfo) {
            const keys: string[] = Object.keys(payload);
            for (const key of keys) {
                state[key] = payload[key];
            }
        }
    },
    getters: {
        isLoggedIn: (state: LoginInfo) => state.isLoggedIn,
        userName: (state: LoginInfo) => state.user?.username
    },
    actions: {
        async checkLogin({ commit, dispatch }: { commit: Commit; dispatch: Dispatch }) {
            const serviceToken = JSON.parse(window.localStorage.getItem('serviceToken') as string);
            const info: LoginInfo = { isInitialized: true, isLoggedIn: false, user: null };
            if (serviceToken) {
                try {
                    dispatch('setSession', serviceToken);
                    const response = await axios.post(`/v1/user/${serviceToken.username}/check`, { username: serviceToken.username });
                    // console.log('response:', response);
                    info.isLoggedIn = true;
                    info.user = (response && response.data && response.data.user) || {};
                } catch ({ status }) {
                    console.error('checkLogin error: ');
                    if (status && status === 401) {
                        dispatch('logout', false);
                        return;
                    }
                }
            }
            commit('update', info);
        },
        setSession(_, serviceToken: ServiceToken) {
            if (serviceToken) {
                localStorage.setItem('serviceToken', JSON.stringify(serviceToken));
                axios.defaults.headers.common['X-Auth-Key'] = `${serviceToken.apikey}`;
            } else {
                localStorage.removeItem('serviceToken');
                delete axios.defaults.headers.common['X-Auth-Key'];
            }
        },
        async login(
            { commit, dispatch, state }: { commit: Commit; dispatch: Dispatch; state: LoginInfo },
            payload: { [key: string]: string }
        ) {
            const { username, password } = payload;
            try {
                const response = await axios.post(`/v1/user/${username}/auth_psk`, { username, password });
                const { apikey, user, time } = response.data;
                commit('update', { ...state, isLoggedIn: true, user });
                dispatch('setSession', { apikey, username: user.username, time });
                dispatch('moveToDashboard');
            } catch (err) {
                const error = err as AxiosError;
                if (error && error.response) {
                    return `${error.message}: ${error.response.data.message}`;
                }
                return String(error);
            }
        },
        async logout({ commit, dispatch, state }: { commit: Commit, dispatch: Dispatch, state: LoginInfo }, force = true) {
            const serviceToken = JSON.parse(window.localStorage.getItem('serviceToken') as string);
            if (serviceToken && force) {
                try {
                    await axios.post(`/v1/user/${serviceToken.username}/deauth`, {
                        username: serviceToken.username,
                        apikey: serviceToken.apikey
                    });
                } catch (e) {
                    console.log('logout error: ', e);
                }
            }
            commit('update', { ...state, isLoggedIn: false, user: null });
            dispatch('setSession', null);
            dispatch('moveToLogin');
        },
        moveToDashboard() {
            router.push({ path: '/dashboard' });
        },
        moveToLogin() {
            router.push({ path: '/login' });
        }
    }
};

export default auth;
