import axios from '@/utils/axios';
import { RestUrl } from '@/utils';
import { HostState, HostEntry } from '@/types';
import { Module } from 'vuex';

const host: Module<HostState, null> = {
    namespaced: true,
    // state: () => ({}),
    mutations: {},
    getters: {},
    actions: {
        async list(): Promise<HostEntry[]> {
            const response = await axios.get(RestUrl.hosts);
            // console.log('getHostList response ', response);
            const {
                data: { resultCode, hostList = [] }
            } = response;

            if (resultCode < 0) {
                throw new Error('Failed to get hosts!');
            }

            // const target = Object.assign({}, hostList[0]);

            // for (let i = 0; i < 62; i++) {
            //     const el = Object.assign({}, target);
            //     el.hostname = `dummy-${i + 1}`;
            //     el.HostUuid = `${el.HostUuid}-$${i}`;
            //     hostList.push(el);
            // }

            hostList.forEach((el: HostEntry) => {
                el.initializeStatus = el.state?.initialize?.status || '';
            });

            return hostList;
        },
        async get({ }, address: string): Promise<HostEntry> {
            const response = await axios.get(`${RestUrl.hosts}/${address}`);
            // console.log('getHostList response ', response);
            const {
                data: { resultCode, host = {} }
            } = response;

            if (resultCode < 0) {
                throw new Error('Failed to get host!');
            }

            return host;
        },
        async add({ }, payload: HostEntry): Promise<void> {
            return await axios.post(RestUrl.hosts, payload);
        },
        async delete({ }, address: string): Promise<void> {
            return await axios.delete(`${RestUrl.hosts}/${address}`);
        }
    }
};

export default host;
