import { createStore } from 'vuex';
import auth from './auth';
import host from './host';
import createPersistedState from 'vuex-persistedstate';

const store = createStore({
    modules: { auth, host },
    plugins: [
        createPersistedState({
            paths: ['auth']
        })
    ]
});

export default store;
