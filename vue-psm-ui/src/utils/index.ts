import axios from './axios';
import i18n from '@/i18n';

export const restBase = '';

export const RestUrl = {
    hosts: `${restBase}/v1/service/host`,
    accounts: `${restBase}/v1/user`,
    pools: `${restBase}/v1/service/storage/pool`,
    cluster: `${restBase}/v1/service/cluster/cloud`,
    sharelink: `${restBase}/v1/service/monitoring/sharelink`,
    log: `${restBase}/v1/service/elasticsearch/log`
};

// export async function loginCheck() {
//     const serviceToken = JSON.parse(window.localStorage.getItem('serviceToken') as string);
//     console.log('init login: serviceToken', serviceToken);
//     const info: LoginInfo = { isInitialized: true, isLoggedIn: false, user: null };
//     if (serviceToken) {
//         setSession(serviceToken);
//         const response = await axios.post(`/v1/user/${serviceToken.username}/check`, { username: serviceToken.username });
//         // console.log('response:', response);
//         info.isLoggedIn = true;
//         info.user = (response && response.data && response.data.user) || {};
//     }
//     console.log('loginCheck end');
//     store.commit('auth/initialize', info);
// }

export async function getStatisticsInfo() {
    const response = await axios.get(`${RestUrl.hosts}/statistics`);
    // console.log('getStatisticsInfo response ', response);
    const { data } = response;
    if (data.resultCode < 0) {
        throw new Error('Failed to getStatisticsInfo!');
    } else {
        return data;
    }
}

export const ipv4Regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;

export const getLabel = (labels: string[], separator = ' ') => {
    return labels.map((el) => `${i18n.global.t(el)}`).join(separator);
}

export const getFieldClassName = (dataName: string, value: string | number) => {
    const dataField = ['status', 'ipmiStatus'];
    if (dataField.includes(dataName)) {
        if (typeof value === 'string') {
            // return `${dataName} ${value.toLowerCase()}`;
            return `status ${value.toLowerCase()}`;
        } else {
            // TODO
        }
    }
    return '';
}