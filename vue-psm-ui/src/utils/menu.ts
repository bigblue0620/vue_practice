import { Menu } from '@/types';
import { mdiViewDashboardOutline, mdiDnsOutline } from '@mdi/js';
// import ShareTwoToneIcon from '@mui/icons-material/ShareTwoTone';
// import StorageTwoToneIcon from '@mui/icons-material/StorageTwoTone';
// import InsertDriveFileTwoToneIcon from '@mui/icons-material/InsertDriveFileTwoTone';
// import PersonOutlineTwoToneIcon from '@mui/icons-material/PersonOutlineTwoTone';

const LNB_MENU: Menu[] = [
    {
        id: '/dashboard',
        type: 'item',
        label: 'dashboard',
        icon: mdiViewDashboardOutline,
    },
    {
        id: '/host',
        type: 'collapse',
        label: 'host',
        icon: mdiDnsOutline,
        children: [
            {
                id: '/host/list',
                type: 'item',
                label: ['host', 'list'],
            },
            {
                id: '/host/network',
                type: 'item',
                label: ['host', 'network'],
            },
            {
                id: '/host/diagram',
                type: 'item',
                label: 'diagram',
            },
            {
                id: '/host/monitoring',
                type: 'item',
                label: 'monitoring',
            },
        ],
    },
];

export default LNB_MENU;
