import axios from 'axios';
import store from '@/store';
import { restBase } from './index';

const axiosInstance = axios.create();
axiosInstance.defaults.baseURL = restBase;
axiosInstance.defaults.withCredentials = true;

axiosInstance.interceptors.request.use(
    (config) => {
        return config;
    },
    (err) => {
        return Promise.reject(err);
    }
);

axiosInstance.interceptors.response.use(
    (response) => {
        // console.log(`response success -----------------------\n${JSON.stringify(response)}`);
        return response;
    },
    (err) => {
        console.log(`response error -----------------------\n${JSON.stringify(err.response)}`);
        if (err.response && err.response.status === 401) {
            store.dispatch('auth/logout');
        }
        return Promise.reject(err);
    }
);

export default axiosInstance;