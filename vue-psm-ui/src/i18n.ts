import { createI18n, LocaleMessageValue, VueMessageType } from 'vue-i18n';

function loadLocaleMessages() {
    const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i);
    const messages: { [index: string]: Record<string, LocaleMessageValue<VueMessageType>> } = {};
    locales.keys().forEach((key) => {
        const matched = key.match(/([A-Za-z0-9-_]+)\./i);
        if (matched && matched.length > 1) {
            const locale = matched[1];
            messages[locale] = locales(key);
        }
    });
    return messages;
}

const locale = window.localStorage.getItem('locale') || 'ko';

const i18n = createI18n({
    locale,
    legacy: false,
    allowComposition: true,
    messages: loadLocaleMessages()
});

export default i18n;
