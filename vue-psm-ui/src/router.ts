import { createWebHistory, createRouter } from 'vue-router';
import Login from '@/pages/Login.vue';
import Dashboard from '@/pages/dashboard/index.vue';
// import HostList from '@/pages/host/HostList.vue';
// import HostNetwork from '@/pages/host/HostNetwork.vue';
// import HostDiagram from '@/pages/host/HostDiagram.vue';
// import HostMonitoring from '@/pages/host/HostMonitoring.vue';
import HostRouters from '@/pages/host/HostRouter';
import Base from '@/components/templates/main/base.vue';
import store from '@/store';

const routes = [
    {
        path: '/login',
        component: Login
    },
    {
        path: '/dashboard',
        component: Base,
        children: [
            {
                path: '',
                component: Dashboard
            }
        ]
    },
    ...HostRouters,
    // {
    //     path: '/host',
    //     component: Base,
    //     children: [
    //         {
    //             path: '/host/list',
    //             component: HostList
    //         },
    //         {
    //             path: '/host/network',
    //             component: HostNetwork
    //         },
    //         {
    //             path: '/host/diagram',
    //             component: HostDiagram
    //         },
    //         {
    //             path: '/host/monitoring',
    //             component: HostMonitoring
    //         }
    //     ]
    // },
    {
        path: '/',
        redirect: '/dashboard'
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from, next) => {
    const isLoggedIn = store.getters['auth/isLoggedIn'];
    if (to.path === '/login') {
        if (isLoggedIn) {
            return next('/dashboard');
        }
    } else {
        if (!isLoggedIn) {
            return next('/login');
        }
    }
    next();
});

export default router;
