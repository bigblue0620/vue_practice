import HostAdd from './HostAdd.vue';
import HostDel from './HostDel.vue';
import { mdiPencil, mdiTrashCan } from '@mdi/js';
import { ActionConfig } from '@/types';

export const HostListActionConfig: ActionConfig = {
    primary: [
        {
            label: ['host', 'add'],
            component: HostAdd,
        },
    ],
    rowAction: {
        type: 'inline',
        actions: [
            {
                key: 'host-modify',
                renderType: 'icon',
                actType: 'dialog',
                component: HostAdd,
                icon: mdiPencil,
                iconBgColor: 'primaryLighten5',
                iconBgHoverColor: 'primary',
                iconColor: 'primary',
                iconHoverColor:  'primaryLighten5',
            },
            {
                key: 'host-delete',
                renderType: 'icon',
                actType: 'dialog',
                component: HostDel,
                icon: mdiTrashCan,
                iconBgColor: 'primaryLighten5',
                iconBgHoverColor: 'primary',
                iconColor: 'error',
                iconHoverColor:  'primaryLighten5',
            },
        ],
    }
}