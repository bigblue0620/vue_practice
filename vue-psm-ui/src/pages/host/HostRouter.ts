import HostList from '@/pages/host/HostList.vue';
import HostNetwork from '@/pages/host/HostNetwork.vue';
import HostDiagram from '@/pages/host/HostDiagram.vue';
import HostMonitoring from '@/pages/host/HostMonitoring.vue';
import Base from '@/components/templates/main/base.vue';

const routes = [
    {
        path: '/host',
        component: Base,
        children: [
            {
                path: '/host/list',
                component: HostList
            },
            {
                path: '/host/network',
                component: HostNetwork
            },
            {
                path: '/host/diagram',
                component: HostDiagram
            },
            {
                path: '/host/monitoring',
                component: HostMonitoring
            }
        ]
    }
];

export default routes;
