module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        hot: true,
        client: {
            webSocketURL: 'ws://192.168.240.130/ws'
        },
        proxy: {
            '/v1': {
                target: 'https://192.168.240.130:8081'
            }
        }
    },

    pluginOptions: {
        i18n: {
            locale: 'ko',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableLegacy: false,
            runtimeOnly: false,
            compositionOnly: false,
            fullInstall: true
        }
    },

    // css: {
    //     loaderOptions: {
    //         sass: {
    //             additionalData: `
    //                 @import "@/assets/scss/settings.scss";
    //             `,
    //         },
    //         scss: {
    //             additionalData: `
    //                 @import "@/assets/scss/settings.scss";
    //             `,
    //         }
    //     }
    // },

    // transpileDependencies: ["vuetify"],
    // css: {
    //     loaderOptions: {
    //         sass: {
    //             implementation: require("sass"),
    //             additionalData: "@import '@/styles/variables.scss'",
    //         },
    //         scss: {
    //             additionalData: "@import '@/styles/variables.scss';",
    //         },
    //     },
    // },

    chainWebpack: (config) => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .tap((options) => {
                options['compilerOptions'] = {
                    ...(options.compilerOptions || {}),
                    isCustomElement: (tag) => tag.startsWith('custom-')
                };
                return options;
            });
    }
};
