module.exports = {
    root: true,
    parser: 'vue-eslint-parser',
    parserOptions: {
        parser: '@typescript-eslint/parser',
        sourceType: 'module',
    },    
    env: {
        browser: true,
    },
    extends: [
        'plugin:vue/base',
        'plugin:vue/vue3-recommended',
        // 'plugin:vue/essential',
        'plugin:@typescript-eslint/recommended',
        'prettier',
   ],
    plugins: [
        "@typescript-eslint",
        'vue',
    ],
    rules: {
        'comma-dangle': [0, 'always-multiline'],
        '@typescript-eslint/explicit-module-boundary-types': 'off',
    },
};
